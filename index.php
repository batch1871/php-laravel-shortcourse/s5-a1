<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity - Session 5</title>
</head>
<body>
	<?php session_start(); ?>

	<form method="POST" action="./server.php">
		Username: <input type="email" name="username" required> <br>
		Password: <input type="password" name="password" required>
		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['username'])): ?>

		<p>Hello, <?= $_SESSION['username']; ?></p>

	<?php endif;?>
</body>
</html>
